import React from 'react'
import ReactDOM from 'react-dom'
import Contents from '../components/Contents'
import Users from '../components/Users'

const data = {
  content: null,
  users: null,
  tags: null,
  colors: ['#8ad475', '#498fe2', '#e36659', '#ffcf3e', '#5c62a4']
}

function fetchApi(endpoint) {
  fetch('http://lessonly-fake-api.herokuapp.com/' + endpoint)
    .then(function(response) {
      return response.json()
    })
    .then(function(json) {
      data[endpoint] = json
      render()
    })
}

function sortByDate() {
  document.getElementById('lessonsByDate').classList.remove('hidden')
  document.getElementById('lessonsAlphabetical').classList.add('hidden')
  document.getElementById('dateSort').classList.add('sortActive')
  document.getElementById('alphaSort').classList.remove('sortActive')
}

function sortAlphabetical() {
  document.getElementById('lessonsByDate').classList.add('hidden')
  document.getElementById('lessonsAlphabetical').classList.remove('hidden')
  document.getElementById('dateSort').classList.remove('sortActive')
  document.getElementById('alphaSort').classList.add('sortActive')
}

function render() {
  if(data.content && data.users && data.tags) {
    var tagData = [['Tag', 'Count']]

    for (var i = 0; i < data.tags.length; i++) {
      tagData[i + 1] = [data.tags[i].name, data.tags[i].count]
    }

    google.charts.load("current", {packages:["corechart"]})
    google.charts.setOnLoadCallback(drawChart)
    function drawChart() {
      var chartData = google.visualization.arrayToDataTable(tagData)

      var options = {
        title: 'Tag Distribution',
        titleTextStyle: {
          fontSize: 20,
          fontName:'Open Sans'
        },
        pieHole: 0.4,
        colors: data.colors,
        fontName: 'Open Sans',
        pieSliceText: 'none'
      }

      var chart = new google.visualization.PieChart(document.getElementById('tagChart'))
      chart.draw(chartData, options)
    }

    document.getElementById('totalLessons').innerHTML = data.content.length

    document.getElementById('totalUsers').innerHTML = data.users.length

    var lessonsCompleted = []
    data.content.forEach(function(content) {
      if (content.stats.completedCount != null){
        lessonsCompleted.push(content.stats.completedCount)
      }
    })
    var totalLessonsCompleted = lessonsCompleted.reduce(function(a, b) {
      return a + b
    }, 0)
    document.getElementById('lessonsCompleted').innerHTML = totalLessonsCompleted

    var scores = []
    data.content.forEach(function(content) {
      if (content.stats.averageScore != null){
        scores.push(content.stats.averageScore)
      }
    })
    var average = scores.reduce(function(a, b) {
      return parseInt(a) + parseInt(b)
    }, 0)
    average /= scores.length
    document.getElementById('averageScore').innerHTML = average + '%'

    ReactDOM.render(
      <div className="container">
        <div className="row">
          <div className="sortGroup">
            <p className="sortBy">Sort lessons by: </p>
            <div className="btn-group" id="sortBtns" role="group">
              <button type="button" className="sortBtn sortActive" id="dateSort"
              onClick={() => sortByDate()}>Date Modified</button>
              <button type="button" className="sortBtn" id="alphaSort"
              onClick={() => sortAlphabetical()}>Alphabetical Order</button>
            </div>
          </div>
          <div className="col-sm-8">
            <h1 className="dataHeader dataHeader-lessons">Lessons</h1>
            <Contents data={data}/>
          </div>
          <div className="col-sm-4">
            <h1 className="dataHeader dataHeader-users">Users</h1>
            <Users data={data}/>
          </div>
        </div>
      </div>
      , document.getElementById('contents')
    )
  }
}

fetchApi('content')
fetchApi('users')
fetchApi('tags')
