import React from 'react'
import User from './User'

var Users = function(props) {
  const UserList = props.data.users.map((users, i) => <User key={i} users={users} data={props.data} />)

  return <div>
    {UserList}
  </div>
}

export default Users
