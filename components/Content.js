import React from 'react'

function userbyID(id, users) {
  var user = users.filter(function(user) {
    return user.id === id
  })

  if (user.length > 0) {
    return user[0].name
  }

  else {
    return 'an anonymous user'
  }
}

function contentTags(ids, tags) {
  var tags = tags.filter(function(tag) {
    return ids.includes(tag.id)
  })
    return tags
}

function capFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1).toLowerCase()
}

var Content = function(props) {
  var lastModified = capFirstLetter(props.content.lastModified.type)
  var username = userbyID(props.content.lastModified.userId, props.data.users)
  var modifiedDate = moment(props.content.lastModified.date).startOf('hour').fromNow()
  var tags = contentTags(props.content.tags, props.data.tags)
  var tagNames = ''
  var averageScore = Math.round(props.content.stats.averageScore)
  var completedCount = props.content.stats.completedCount
  var lastCompletedDate = moment(props.content.stats.lastCompleted).startOf('hour').fromNow()
  var lessonStyle = {
    backgroundColor: '#8CBFFF',
    height: 10,
    margin: 0
  }

  if (tags.length > 0) {
    tags.forEach(function(tag) {
      if (tags[tags.length-1].id == tag.id) {
        tagNames = tagNames + tag.name
      }
      else {
        tagNames = tagNames + tag.name + ', '
      }
    })
  }
  else {
    tagNames = []
  }

  if (props.content.stats.completedCount.length > 1) {
    completedCount = competedCount + ' users'
  }
  else {
    completedCount = completedCount + ' user'
  }

  return <div>
    <div className="coloredCap coloredCap-lesson" style={lessonStyle}></div>
    <div className="lesson">
      <a href={props.content.urls.overview}>
        <h2 className="lesson-title">
          {props.content.title}
        </h2>
      </a>
      <div className="lesson-modifiers">
        {props.content.urls.edit ? <a className="lesson-edit-link" href={props.content.urls.edit} title="Edit the lesson"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></a> : null}
        {props.content.urls.share ? <a className="lesson-share-link" href={props.content.urls.share} title="Share the lesson"><i className="fa fa-share-square-o" aria-hidden="true"></i></a> : null}
      </div>
      {props.content.stats.averageScore || props.content.stats.completedCount || props.content.stats.lastCompleted ?
        <div className="lesson-stats hidden-xs">
          <table className="lesson-stats-table">
            <tbody>
            <tr>
              {props.content.stats.averageScore ? <th>Average Score</th> : null}
              {props.content.stats.completedCount ? <th>Completed By</th> : null}
              {props.content.stats.lastCompleted ? <th>Last Completed</th> : null}
            </tr>
            <tr>
              {props.content.stats.averageScore ? <td>{averageScore}/100</td> : null}
              {props.content.stats.completedCount ? <td>{completedCount}</td> : null}
              {props.content.stats.lastCompleted ? <td>{lastCompletedDate}</td> : null}
            </tr>
            </tbody>
          </table>
        </div>
        : null
      }
      <div className="lesson-bottom">
        <h6 className="lesson-modified-info">{lastModified} {modifiedDate} by {username}</h6>
        <h5 className="lesson-tag text-right hidden-xs">{tagNames}</h5>
      </div>
    </div>
  </div>
}

export default Content
