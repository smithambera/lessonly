import React from 'react'

function lessonModifications(id, contents) {
  var userModifications = contents.filter(function(content) {
    return content.lastModified.userId === id
  })
  return userModifications
}

var User = function(props) {
  var lessonsModified = lessonModifications(props.users.id, props.data.content)
  var lessonTitles = []

  if (lessonsModified.length > 0) {
    lessonsModified.forEach(function(lesson) {
      lessonTitles.push(<li>{lesson.lastModified.type + ' '} <a href={lesson.urls.overview}>{lesson.title}</a> {' ' + moment(lesson.lastModified.date).startOf('hour').fromNow()}</li>)
    })
  }

  var userStyle = {
    backgroundColor: '#FFE28C',
    height: 10,
    margin: 0
  }

  return <div>
    <div className="coloredCap" style={userStyle}></div>
    <div className="user">
      <h3 className="user-name">
        {props.users.name}
      </h3>
      <ul className="userLessonModifications">
        {lessonTitles}
      </ul>
    </div>
  </div>
}

export default User
