import React from 'react'
import Content from './Content'

var Contents = function(props) {

  var sortedByDate = props.data.content.sort(function(a, b) {
    var dateA = moment(a.lastModified.date)
    return (dateA.isBefore(b.lastModified.date)) ? 1 : (dateA.isAfter(b.lastModified.date) ? -1 : 0)
  }).map((content, i) => <Content key={i} content={content} data={props.data} />)

  var sortedAlphabetical = props.data.content.sort(function(a, b) {
    if (a.title < b.title)
      return -1
    if (a.title > b.title)
      return 1
    return 0
  }).map((content, i) => <Content key={i} content={content} data={props.data} />)

  return <div>
    <section id="lessonsByDate">{sortedByDate}</section>
    <section id="lessonsAlphabetical" className="hidden">{sortedAlphabetical}</section>
  </div>
}

export default Contents
